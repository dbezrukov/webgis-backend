var mongoose = require(__coreModules + '/mongoose');
var Schema = mongoose.Schema;

var schema = mongoose.Schema({
    user: { type: Schema.ObjectId,  ref: 'User' },
    name: String,
    description: String,
    placeName: String,
    date: String, // YYMMDD
    startUtc: String, // HHMM
    duration: String, // HHMM
    zoneLat: Number, // km
    zoneLon: Number,
    zoneCenter: String,
    zoneRadius: Number,
    hgtAMSL: Number, // absolute, meters
    hgtAGL: Number, // relative, meters
    company: String,
    fcoName: String,
    fcoPhone: String,
    pilotName: String,
    pilotPhone: String,
    drones: [{ type: Schema.ObjectId, ref: 'Drone' }],
    created: {
        type: Number,
        default: Date.now
    }
});

module.exports = mongoose.model('FlightRequest', schema);