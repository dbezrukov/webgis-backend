var mongoose = require(__coreModules + '/mongoose');
var Schema = mongoose.Schema;

/* Хранить здесь доп. информацию о цели */

var schema = mongoose.Schema({

    id: {
        type: String,
        default: ''
    },
    type: { 
        type: String 
    },
    geometry: {
        type: Schema.Types.Mixed,
        default: {}
    },
    properties: {
        type: Schema.Types.Mixed,
        default: {}
    },
    updated: {
        type: Number,
        default: Date.now
    }
});

const layers = [
    {
        name: 'ez', // Запретные зоны, http://aopa.ru/maps/204_v02.kmz
    },
    {
        name: 'dz', // Опасные зоны, http://aopa.ru/maps/203_v02.kmz
    },
    {
        name: 'lz', // Зоны ограничения полётов, http://aopa.ru/maps/212_v02.kmz
    },
    {
        name: 'ad', // Аэродромы, http://aopa.ru/maps/ad_v02.kmz
        filter: {
            "geometry.type": "Point",
            "properties.styleUrl": "#AD"
        }
    },
    {
        name: 'vd', // Вертодромы, http://aopa.ru/maps/ad_v02.kmz
        filter: {
            "geometry.type": "Point",
            "properties.styleUrl": "#VD"
        }
    }
];

let models = layers.reduce((total, layer) => {
    total[layer.name] = mongoose.model(`Feature${layer.name}`, schema, `features_${layer.name}`);
    total[layer.name].filter = layer.filter;
    return total;
}, {});

module.exports = {
    Features : models
}

