var mongoose = require(__coreModules + '/mongoose');
var Schema = mongoose.Schema;

/* Хранить здесь доп. информацию о цели */

var targetInfoSchema = mongoose.Schema({

    id: {
        type: String,
        default: ''
    },
    updated: {
        type: Number,
        default: 0
    },
    data: {
        type: Schema.Types.Mixed,
        default: {}
    }
});

module.exports = mongoose.model('TargetInfo', targetInfoSchema);