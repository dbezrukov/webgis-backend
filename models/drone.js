var mongoose = require(__coreModules + '/mongoose');
var Schema = mongoose.Schema;

var schema = mongoose.Schema({
    user: { type: Schema.ObjectId,  ref: 'User' },
    model: String,
    type: String,
    serial: String,
    weight: Number,
    photo : { type: String, default: 'drones/default' },
});

module.exports = mongoose.model('Drone', schema);