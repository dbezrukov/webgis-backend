var _ = require(__coreModules + '/underscore');
var _str = require(__coreModules + '/underscore.string');
var async = require(__coreModules + '/async');

var Drone = require('../models/drone');

module.exports = function(app, passport) {
    
    // Create an drone
    app.post('/api/drones', function(req, res) {
        
        async.waterfall([
            function(done) {
                console.log('#create drone');

                var drone = new Drone(req.body);

                drone.save(function(err) {
                    done(err, drone);
                });
            },
        ], function(err, drone) {
            if (err) {
                console.log('#error: ' + err);
                return res.json(500, { error: err });
            }
            
            console.log('#returning new drone data');

            res.json(drone.toJSON());
        });
    })

    // Get all drones
    app.get('/api/drones', function(req, res) {
        
        Drone
            .find()
            .sort({
                created: 'asc'
            })
            .exec(function(err, drones) {
            
            if (err) {
                res.json(500, { error: 'db error' });
                return;
            }

            res.json(drones);
        });
    })

    // Get the drone
    app.get('/api/drones/:id', function(req, res) {

        var droneId = req.params.id;

        Drone
            .findById(droneId)
            .exec(function(err, drone) {
            
            if (err) {
                res.json(500, { error: err });
                return;
            }

            if (!drone) {
                res.json({});
                return;
            }

            res.json(drone);
        });
    });

    // Update the drone
    app.post('/api/drones/:id', function(req, res) {

        var droneId = req.params.id;

        Drone
            .findById(droneId)
            .exec(function(err, drone) {
            
            if (err) {
                res.json(500, { error: err });
                return;
            }

            if (!drone) {
                res.json(401, { error: 'drone not found' });
                return;
            }

            Drone
                .findByIdAndUpdate(droneId, { $set : req.body }, { new: true })
                .exec(function(err, updatedData) {
                
                if (err) {
                    res.json(500, { error: err });
                    return;
                }

                res.json(updatedData.toJSON());
            });

        });
    });

    // Delete the drone
    app.delete('/api/drones/:id', function(req, res) {

        var droneId = req.params.id;

        Drone
            .findByIdAndRemove(droneId)
            .exec(function(err, drone) {
            
            if (err) {
                res.json(500, { error: err });
                return;
            }

            res.json({});
        });
    });
};
