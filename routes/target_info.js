var _ = require(__coreModules + '/underscore');
var async = require(__coreModules + '/async');

var TargetInfo = require('../models/target_info');

module.exports = function(app, passport) {

    app.post('/api/target-info/:id', function(req, res) {

        var targetId = req.params.id;
        var targetInfoData = req.body;

        var targetInfo = new TargetInfo({
            id: targetId,
            updated: Date.now(),
            data: targetInfoData
        });

        targetInfo.save(function(err) {

            if (err) {
                return res.json(500, {
                    error: err
                });
            }

            res.json({});
        });
    });

    app.get('/api/target-info/:id', function(req, res) {

        var targetId = req.params.id;

        TargetInfo
            .find({
                id: targetId
            })
            .select({
                '_id': 0,
                '__v': 0
            })
            .sort({
                updated: -1
            })
            .limit(1)
            .exec(function(err, targetInfo) {

                res.json(targetInfo[0] || {});
            })
    });
};

// delete old records
setInterval(function() {

    var now = Date.now();
    var locationTimeout = 186 * 24 * 60 * 1 * 60 * 1000; // 186 days

    var query = {
        'updated': {
            $lt: now - locationTimeout
        }
    }

    TargetInfo.remove(query, function(err, result) {
        if (err) {
            console.log('Error while removing old records');
            return;
        }
    });

}, 20 * 60000); // 20 minutes