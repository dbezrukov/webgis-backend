const { Features } = require('../models/feature');

module.exports = function(app, passport) {

    app.get('/api/features/:layer', function(req, res) {

        console.log(`searching features from layer: ${req.params.layer}`);

        const layer = req.params.layer;

        let model = Features[layer];
        if (!model) {
            console.log(`no model for layer ${layer}`);
            res.status(404).send({
                error: 'no such layer'
            });
            return;
        }

        const { fNBnd, fEBnd, fSBnd, fWBnd } = req.query;

        let conditions = [];

        conditions.push({
            geometry: {
                $geoIntersects: {
                    $geometry: {
                        type : "Polygon" ,
                        coordinates: [[ [ -100, 60 ], [ -100, 0 ], [ 100, -0 ], [ 100, 60 ], [ -100, 60 ] ]],
                        crs: {
                            type: "name",
                            properties: { name: "urn:x-mongodb:crs:strictwinding:EPSG:4326" }
                        }
                    }
                },
            }
        })

        if (model.filter) {
            console.log(JSON.stringify(model.filter));
            conditions.push(model.filter);
        }

        model
            .find({ $and: conditions })
            .exec(function(err, features) {

                if (err) {
                    res.status(500).send({
                        error: err
                    });
                    return;
                }

                console.log(`done, found ${features.length} features`);

                res.json({
                    features: features,
                    stm: Date.now()
                });
            });
    });
};

