var _ = require(__coreModules + '/underscore');
var _str = require(__coreModules + '/underscore.string');
var async = require(__coreModules + '/async');

var Request = require('../models/flightrequest');

module.exports = function(app, passport) {
    
    // Create an flightrequest
    app.post('/api/flightrequests', function(req, res) {
        
        async.waterfall([
            function(done) {
                console.log('#create flightrequest');

                var flightrequest = new Request(req.body);

                flightrequest.save(function(err) {
                    done(err, flightrequest);
                });
            },
        ], function(err, flightrequest) {
            if (err) {
                console.log('#error: ' + err);
                return res.json(500, { error: err });
            }
            
            console.log('#returning new flightrequest data');

            res.json(flightrequest.toJSON());
        });
    })

    // Get all flightrequests
    app.get('/api/flightrequests', function(req, res) {
        
        Request
            .find()
            .sort({
                created: 'asc'
            })
            .exec(function(err, flightrequests) {
            
            if (err) {
                res.json(500, { error: 'db error' });
                return;
            }

            res.json(flightrequests);
        });
    })

    // Get the flightrequest
    app.get('/api/flightrequests/:id', function(req, res) {

        var flightrequestId = req.params.id;

        Request
            .findById(flightrequestId)
            .exec(function(err, flightrequest) {
            
            if (err) {
                res.json(500, { error: err });
                return;
            }

            if (!flightrequest) {
                res.json({});
                return;
            }

            res.json(flightrequest);
        });
    });

    // Update the flightrequest
    app.post('/api/flightrequests/:id', function(req, res) {

        var flightrequestId = req.params.id;

        Request
            .findById(flightrequestId)
            .exec(function(err, flightrequest) {
            
            if (err) {
                res.json(500, { error: err });
                return;
            }

            if (!flightrequest) {
                res.json(401, { error: 'flightrequest not found' });
                return;
            }

            Request
                .findByIdAndUpdate(flightrequestId, { $set : req.body }, { new: true })
                .exec(function(err, updatedData) {
                
                if (err) {
                    res.json(500, { error: err });
                    return;
                }

                res.json(updatedData.toJSON());
            });

        });
    });

    // Delete the flightrequest
    app.delete('/api/flightrequests/:id', function(req, res) {

        var flightrequestId = req.params.id;

        Request
            .findByIdAndRemove(flightrequestId)
            .exec(function(err, flightrequest) {
            
            if (err) {
                res.json(500, { error: err });
                return;
            }

            res.json({});
        });
    });
};
