mkdir $(date +%Y%m%d)
cd $(date +%Y%m%d)

wget http://aopa.ru/maps/root_v02.kmz
unzip -p root_v02.kmz > doc.kml

# download 
for f in $(xmlstarlet sel -N x="http://www.opengis.net/kml/2.2" -t -v "/x:kml/x:Document/x:NetworkLink/x:Link/x:href" doc.kml)
do
   wget $f
done

# extract
for f in *.kmz
do 
    unzip -p "$f" > "${f%.*}.kml"
done

# convert
for f in *.kml
do 
    echo "converting $f"
    togeojson "$f" > "${f%.*}.geojson"
done