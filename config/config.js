module.exports = {

    app: {
        name: 'DroneRadar',
	    url: "https://droneradar.org",
        filestorage: 'https://res.cloudinary.com/naturometer/image/upload/',
        debug: false // enable API logging,
    },

    auth: {
        exposeUsers: false
    },

    session: {
        name: 'droneradar.sid',
        secret: 'droneradar',
    },

    server: {
        port: process.env.PORT || 3070
    },

    db: 'mongodb://localhost/droneradar', // using default mongodb port 27019

    cloudinary: {
        cloud_name: 'naturometer',
        api_key: '771139794262174',
        api_secret: 'HPJcCKOfuZHiOi5aM9aWEDM2Ei8'
    },

    /*
    facebook: {
        "app_id":"1213091548838919",
        "app_secret":"99c5a2df0384b11756dd1e72081121b0",
        "callback":"https://droneradar.org/api/auth/facebook/callback"
    },

    vk: {
        "app_id":"6485432",
        "app_secret":"xVgfAh0aeJw7HCKb8gBg",
        "callback":"https://droneradar.org/api/auth/vk/callback"
    },
    */

    mailer: {
        user: 'info@droneradar.org',
        password: 'ssDRONERADAR12345',
        host: 'smtp.mail.ru',
        ssl: true,
        from: 'DroneRadar <info@droneradar.org>',
        sign: '--<br>С уважением,<br><a href="https://droneradar.org">https://droneradar.org</a>'
    },

    raven: {
        dsn: 'https://648ef86f3e0c4a3d9ac7ade38de46d22:2f9c8b9d371e4adaa709204a63130c5c@sentry.io/261306'
    }
}

